package com.dizzer.testtaskappa.model;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import static com.dizzer.testtaskappa.constant.CustomConstant.SORT_ORDER_BY_STATUS;
import static com.dizzer.testtaskappa.constant.CustomConstant.SORT_ORDER_BY_TIME;
import static com.dizzer.testtaskappa.db.Contract.CONTENT_URI_IMAGES;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_ID;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_LINK;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_STATUS;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_TIME;

public class LinkListModel implements ILinkListModel {
    @Override
    public ArrayList<LinkModel> getLinks(int sortOrder, Context mContext) {
        ArrayList<LinkModel> linkModels;
        Cursor cursor;
        switch (sortOrder){
            case SORT_ORDER_BY_TIME:
                cursor = mContext.getContentResolver().query(CONTENT_URI_IMAGES, null, null, null,"time DESC");
                break;
            case SORT_ORDER_BY_STATUS:
                cursor = mContext.getContentResolver().query(CONTENT_URI_IMAGES, null, null, null,"status ASC");
                break;
            default:
                cursor = mContext.getContentResolver().query(CONTENT_URI_IMAGES, null, null, null, null);
        }
        linkModels = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {

            do {
                long id = cursor.getLong(cursor.getColumnIndex(IMAGE_ID));
                String link = cursor.getString(cursor.getColumnIndex(IMAGE_LINK));
                int status = cursor.getInt(cursor.getColumnIndex(IMAGE_STATUS));
                long time = cursor.getLong(cursor.getColumnIndex(IMAGE_TIME));

                LinkModel linkModel = new LinkModel(id, link, status, time);
                linkModels.add(linkModel);

            } while (cursor.moveToNext());
            cursor.close();
        }

        return linkModels;
    }
}
