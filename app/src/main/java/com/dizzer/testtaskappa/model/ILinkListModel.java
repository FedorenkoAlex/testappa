package com.dizzer.testtaskappa.model;

import android.content.Context;

import java.util.List;

public interface ILinkListModel {
    List<LinkModel> getLinks(int sortOrder, Context mContext);
}
