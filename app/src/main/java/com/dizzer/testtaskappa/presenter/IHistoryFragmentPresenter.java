package com.dizzer.testtaskappa.presenter;

import com.dizzer.testtaskappa.model.LinkModel;

import java.util.ArrayList;

public interface IHistoryFragmentPresenter {
    ArrayList<LinkModel> setDataForRecyclerView(int sortOrder);

    void onItemClick(int position);
}
