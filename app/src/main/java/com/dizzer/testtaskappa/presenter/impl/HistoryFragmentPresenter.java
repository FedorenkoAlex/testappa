package com.dizzer.testtaskappa.presenter.impl;

import android.content.Context;

import com.dizzer.testtaskappa.model.LinkListModel;
import com.dizzer.testtaskappa.model.LinkModel;
import com.dizzer.testtaskappa.presenter.IHistoryFragmentPresenter;
import com.dizzer.testtaskappa.view.contractView.HistoryFragmentView;

import java.util.ArrayList;

public class HistoryFragmentPresenter implements IHistoryFragmentPresenter {

    private HistoryFragmentView view;
    private LinkListModel listModel;
    private Context mContext;
    private ArrayList<LinkModel> linkModelsList;

    public HistoryFragmentPresenter(HistoryFragmentView view, Context mContext) {
        this.view = view;
        listModel = new LinkListModel();
        this.mContext = mContext;
    }

    @Override
    public ArrayList<LinkModel> setDataForRecyclerView(int sortOrder) {
        linkModelsList = listModel.getLinks(sortOrder, mContext);
        return listModel.getLinks(sortOrder, mContext);
    }

    @Override
    public void onItemClick(int position) {
        String linkAddress = linkModelsList.get(position).getLink();
        int linkStatus = linkModelsList.get(position).getStatus();
        long id = linkModelsList.get(position).getLinkID();
        view.startAppB(linkAddress, linkStatus, id);
    }
}
