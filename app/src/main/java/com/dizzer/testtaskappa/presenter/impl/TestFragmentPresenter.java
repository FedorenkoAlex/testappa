package com.dizzer.testtaskappa.presenter.impl;

import android.content.Context;

import com.dizzer.testtaskappa.R;
import com.dizzer.testtaskappa.presenter.ITestFragmentPresenter;
import com.dizzer.testtaskappa.view.contractView.TestFragmentView;

public class TestFragmentPresenter implements ITestFragmentPresenter {

    private TestFragmentView view;
    private Context context;

    public TestFragmentPresenter(TestFragmentView view, Context context) {
        this.view = view;
        this.context = context;
    }


    @Override
    public void onAddLinkBtn(String link) {
        String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        if (link.matches(regex)) {
            view.loadAppB(link);
        } else
            view.showMessage(context.getResources().getString(R.string.incorrect_link));
    }
}
