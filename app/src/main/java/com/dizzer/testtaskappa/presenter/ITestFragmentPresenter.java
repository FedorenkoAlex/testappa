package com.dizzer.testtaskappa.presenter;

public interface ITestFragmentPresenter {
    void onAddLinkBtn(String link);
}
