package com.dizzer.testtaskappa.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.dizzer.testtaskappa.db.Contract.CONTENT_AUTHORITY;
import static com.dizzer.testtaskappa.db.Contract.IMAGES_PATH;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_TABLE_NAME;

public class CustomProvider extends ContentProvider {
    private static final int IMAGE = 1001;
    private static final int IMAGE_ID = 1002;

    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(CONTENT_AUTHORITY,IMAGES_PATH,IMAGE);
        uriMatcher.addURI(CONTENT_AUTHORITY,IMAGES_PATH +"/*",IMAGE_ID);
    }

    private CustomDB mCustomDB;

    @Override
    public boolean onCreate() {
        mCustomDB = new CustomDB(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        SQLiteDatabase database = mCustomDB.getReadableDatabase();
        int mUriMather = uriMatcher.match(uri);
        Cursor cursor;
        Context context = getContext();
        assert context != null;
        switch (mUriMather) {
            case IMAGE:
                cursor = database.query(IMAGE_TABLE_NAME, strings, s, strings1, null, null, s1);
                cursor.setNotificationUri(context.getContentResolver(), uri);
                return cursor;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        SQLiteDatabase database = mCustomDB.getWritableDatabase();
        int mUriMather = uriMatcher.match(uri);
        Uri resultUri;
        Context context = getContext();
        assert context != null;
        switch (mUriMather) {
            case IMAGE:
                long insertImage = database.insert(IMAGE_TABLE_NAME, null, contentValues);
                resultUri = ContentUris.withAppendedId(uri,insertImage);
                context.getContentResolver().notifyChange(uri, null, false);
                return resultUri;
            default:
                return null;
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        SQLiteDatabase database = mCustomDB.getWritableDatabase();
        int mUriMather = uriMatcher.match(uri);
        int resultDelete = -1;
        Context context = getContext();
        assert context != null;
        switch (mUriMather) {
            case IMAGE_ID:
                s = "_id = ?";
                int idDelete = Integer.parseInt(uri.getLastPathSegment());
                strings = new String[]{String.valueOf(idDelete)};
                resultDelete = database.delete(IMAGE_TABLE_NAME, s, strings);
                context.getContentResolver().notifyChange(uri, null, false);
                return resultDelete;
            case IMAGE:
                resultDelete = database.delete(IMAGE_TABLE_NAME, s, strings);
                context.getContentResolver().notifyChange(uri, null, false);
                return resultDelete;
            default:
                return resultDelete;
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        SQLiteDatabase database = mCustomDB.getWritableDatabase();
        int mUriMather = uriMatcher.match(uri);
        int resultUpdate = -1;
        Context context = getContext();
        assert context != null;
        switch (mUriMather) {
            case IMAGE:
                resultUpdate = database.update(IMAGE_TABLE_NAME, contentValues, s, strings);
                context.getContentResolver().notifyChange(uri, null, false);
                return resultUpdate;
            default:
                return resultUpdate;
        }
    }
}
