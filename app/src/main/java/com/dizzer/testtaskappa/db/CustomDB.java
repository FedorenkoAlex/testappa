package com.dizzer.testtaskappa.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.dizzer.testtaskappa.db.Contract.IMAGE_ID;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_LINK;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_STATUS;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_TABLE_NAME;
import static com.dizzer.testtaskappa.db.Contract.IMAGE_TIME;

public class CustomDB extends SQLiteOpenHelper {

    private static final String DB_NAME = "AppA.db";
    private static final int DB_VERSION = 1;

    private static final String SQL_CREATE_TABLE_IMAGE = "CREATE TABLE " + IMAGE_TABLE_NAME
            + " ("
            + IMAGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + IMAGE_LINK + " TEXT,"
            + IMAGE_STATUS + " INTEGER,"
            + IMAGE_TIME + " INTEGER)";

    private static final String SQL_DROP_TABLE_IMAGE = "DROP TABLE " + IMAGE_TABLE_NAME;

    public CustomDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_IMAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DROP_TABLE_IMAGE);
        onCreate(sqLiteDatabase);
    }
}