package com.dizzer.testtaskappa.db;

import android.net.Uri;

public class Contract {

    static final String IMAGE_TABLE_NAME = "images";

    public static final String IMAGE_ID = "_id";
    public static final String IMAGE_LINK = "link";
    public static final String IMAGE_STATUS = "status";
    public static final String IMAGE_TIME = "time";

    static final String CONTENT_AUTHORITY = "com.dizzer.testtaskappa.db";
    static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    static final String IMAGES_PATH = "images";
    public static final Uri CONTENT_URI_IMAGES = BASE_CONTENT_URI.buildUpon().appendPath(IMAGES_PATH).build();
}
