package com.dizzer.testtaskappa.adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dizzer.testtaskappa.R;
import com.dizzer.testtaskappa.constant.CustomConstant;
import com.dizzer.testtaskappa.model.LinkModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private List<LinkModel> linkModels;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public RecyclerViewAdapter(List<LinkModel> linkModels, OnItemClickListener mOnItemClickListener) {
        this.linkModels = linkModels;
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Date date = new Date(linkModels.get(i).getTime());
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("E yyyy.MM.dd 'time :' hh:mm:ss a zzz");
        int status = linkModels.get(i).getStatus();

        viewHolder.rviLinkDate.setText(formatForDateNow.format(date));
        viewHolder.rviLinkAddress.setText(linkModels.get(i).getLink());

        switch(status){
            case CustomConstant.STATUS_DOWNLOADED:
                viewHolder.view.setBackgroundColor(Color.GREEN);
                break;
            case CustomConstant.STATUS_ERROR:
                viewHolder.view.setBackgroundColor(Color.RED);
                break;
            default:
                viewHolder.view.setBackgroundColor(Color.GRAY);
        }
    }

    @Override
    public int getItemCount() {
        return linkModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.rviLinkAddress)
        TextView rviLinkAddress;
        @BindView(R.id.rviLinkDate)
        TextView rviLinkDate;

        private View view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = getLayoutPosition();
            mOnItemClickListener.onItemClick(itemPosition);
        }
    }
}
