package com.dizzer.testtaskappa.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.dizzer.testtaskappa.R;
import com.dizzer.testtaskappa.constant.CustomConstant;
import com.dizzer.testtaskappa.presenter.ITestFragmentPresenter;
import com.dizzer.testtaskappa.presenter.impl.TestFragmentPresenter;
import com.dizzer.testtaskappa.view.contractView.TestFragmentView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class TestFragment extends Fragment implements TestFragmentView {

    @BindView(R.id.etLinkTestTab)
    EditText etLinkTestTab;
    Unbinder unbinder;
    private ITestFragmentPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.test_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new TestFragmentPresenter(this, getActivity());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnOkTestTab)
    public void okBtnClick() {
        presenter.onAddLinkBtn(etLinkTestTab.getText().toString());
    }

    @Override
    public void loadAppB(String link) {
        Intent intent = new Intent("com.dizzer.testtaskappb.intent.filter.for.appA");
        intent.putExtra(CustomConstant.INTENT_CODE, CustomConstant.TEST_FRAGMENT);
        intent.putExtra(CustomConstant.INTENT_LINK, link);
        startActivity(intent);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

}
