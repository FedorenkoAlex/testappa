package com.dizzer.testtaskappa.view;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dizzer.testtaskappa.R;
import com.dizzer.testtaskappa.adapters.RecyclerViewAdapter;
import com.dizzer.testtaskappa.constant.CustomConstant;
import com.dizzer.testtaskappa.presenter.IHistoryFragmentPresenter;
import com.dizzer.testtaskappa.presenter.impl.HistoryFragmentPresenter;
import com.dizzer.testtaskappa.view.contractView.HistoryFragmentView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.dizzer.testtaskappa.constant.CustomConstant.HISTORY_TAB_FRAGMENT_LOADER;
import static com.dizzer.testtaskappa.constant.CustomConstant.SORT_ORDER_BY_STATUS;
import static com.dizzer.testtaskappa.constant.CustomConstant.SORT_ORDER_BY_TIME;
import static com.dizzer.testtaskappa.db.Contract.CONTENT_URI_IMAGES;

public class HistoryFragment extends Fragment implements HistoryFragmentView, LoaderManager.LoaderCallbacks<Cursor>, RecyclerViewAdapter.OnItemClickListener {

    @BindView(R.id.recyclerViewHistoryTab)
    RecyclerView recyclerViewHistoryTab;
    Unbinder unbinder;

    private RecyclerViewAdapter adapter;
    private IHistoryFragmentPresenter presenter;
    private int sortOrder = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter = new HistoryFragmentPresenter(this, getActivity());

        recyclerViewHistoryTab.setLayoutManager(new LinearLayoutManager(getActivity()));

        getLoaderManager().initLoader(HISTORY_TAB_FRAGMENT_LOADER, null, this);

        setRecyclerViewAdapter();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), CONTENT_URI_IMAGES, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        setRecyclerViewAdapter();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }


    @Override
    public void onItemClick(int position) {
        presenter.onItemClick(position);
    }

    @Override
    public void startAppB(String linkAddress, int linkStatus, long id) {
        Intent intent = new Intent("com.dizzer.testtaskappb.intent.filter.for.appA");
        intent.putExtra(CustomConstant.INTENT_CODE,CustomConstant.HISTORY_FRAGMENT);
        intent.putExtra(CustomConstant.INTENT_LINK, linkAddress);
        intent.putExtra(CustomConstant.INTENT_STATUS, linkStatus);
        intent.putExtra(CustomConstant.INTENT_ID, id);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_items,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_by_date:
                sortOrder = SORT_ORDER_BY_TIME;
                setRecyclerViewAdapter();
                break;
            case R.id.menu_item_by_status:
                sortOrder = SORT_ORDER_BY_STATUS;
                setRecyclerViewAdapter();
        }
        return true;
    }

    private void setRecyclerViewAdapter() {
        adapter = new RecyclerViewAdapter(presenter.setDataForRecyclerView(sortOrder), this);
        recyclerViewHistoryTab.setAdapter(adapter);
    }
}
