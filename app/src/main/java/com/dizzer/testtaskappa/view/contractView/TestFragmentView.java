package com.dizzer.testtaskappa.view.contractView;

public interface TestFragmentView {
    void loadAppB(String link);

    void showMessage(String msg);
}
