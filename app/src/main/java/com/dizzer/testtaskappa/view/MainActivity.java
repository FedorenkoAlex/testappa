package com.dizzer.testtaskappa.view;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.dizzer.testtaskappa.R;
import com.dizzer.testtaskappa.adapters.PagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setViewPager();
    }

    private void setViewPager(){
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), this);
        adapter.addFragment(new TestFragment(), getResources().getString(R.string.test_tab_title));
        adapter.addFragment(new HistoryFragment(), getResources().getString(R.string.history_tab_title));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
